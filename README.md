<!--
 * @Author: hua
 * @Date: 2019-02-01 13:57:47
 * @LastEditors: hua
 * @LastEditTime: 2019-11-14 11:08:35
 -->
### 工具鸡-聊天室
    兼容web,android,ios的聊天室。一次开发多端使用。

### 体验地址
http://im.zhuhui.store    

### 安卓版下载地址
http://down.zhuhui.store/chat.apk

### 文档地址
http://doc.zhuhui.store

### 项目架构
    app是前端,基于vue开发，
    chatAdmin是后台，基于vue开发，
    chatApi是接口，基于flask开发，
    前后端完全分离项目，适用于多端聊天应用。

### 前端项目功能
- [x] 登录注册
- [x] 用户界面
- [x] 设置界面
- [x] 聊天消息列表
- [x] 联系人列表
- [x] 聊天界面
- [x] 群聊
- [x] 聊天未收到重发
- [x] 增加聊天记录云端存储
- [x] rsa加密数据

### 后台项目功能
- [x] 登录
- [x] 房间管理
- [x] 通讯录管理
- [x] 用户管理
- [x] 管理员管理

### 项目截图
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/142434_ce5fed5e_1588193.png "8AO2N23X(AT%YCKU~)ZDICY.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/142442_df240c6e_1588193.png "8XFNDJCM46U)VSCZNI~(MZW.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/142449_b130cf60_1588193.png "153}QG8F60OV8HI27ZDMSN6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/142458_11de22a5_1588193.png "C}EI)WI9VH$GD~XK15IH}}5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/142505_7fc25269_1588193.png "K~2G@NU~8WZG7WR0`FGS2]H.png")

### 前端项目安装
    cnpm install

### 前端项目开发环境
    npm run dev

### 前端项目生产环境
    npm run build

### 后端项目安装
    1.运行环境python3.5+, mysql
    2.数据库在doc目录下
    3.安装扩展
        pip install -r requirements.txt
    4.运行项目
        python socketRun.py

#### 作者其他开源产品
1. <a href="https://gitee.com/huashiyuting/flask " target="_blank">mvc分层,json api载体(中庸)的flask</a>
2. <a href="https://gitee.com/huashiyuting/tool_chicken" target="_blank">工具鸡前端app项目 </a>
3. <a href="https://gitee.com/huashiyuting/status_bar_monitor" target="_blank">状态栏监听安卓客户端 </a>
4. <a href="https://gitee.com/huashiyuting/plainCms" target="_blank">plainCms</a>

#### 群内交流
![群内交流](https://images.gitee.com/uploads/images/2019/0724/121735_03ee3000_1588193.png "temp_qrcode_share_566438779.png")

#### 捐助作者
![捐助作者](https://images.gitee.com/uploads/images/2019/0124/105407_661d1190_1588193.png "mm_facetoface_collect_qrcode_1548297043215.png")	
### 购买实时最新版
[淘宝链接](https://item.taobao.com/item.htm?id=607024716747 ).

### 二次开发

    如果你对此项目有什么好的想法，可以联系作者定制开发。
   
